# FoundryVTT lang ar-AR
This module adds the option to select the Arabic (العربية) language from the FoundryVTT settings menu. Selecting this option will translate various aspects of the program interface.
## Installation
In the Add-On Modules option click on Install Module and place the following link in the field Manifest URL

https://gitlab.com/mr_aljabry/foundryvtt-lang-ar-ar/-/raw/master/Core/module.json

If this option does not work download the core-AR.zip file and extract its contents into the /resources/app/public/modules/ folder
Once this is done, enable the module in the settings of the world in which you intend to use it and then change the language in the settings.

# اللغة العربية لطاولة فاوندري الافتراضية
هذه الوحدة تضيف خيار اختيار اللغة العربية من قائمة اعدادات طاولة فاوندري الافتراضية. اختيار هذاالخيار يوف يترجم عدة جوانب من واجهة البرنامج.
## التثبيت
في خيار الوحدات الملحقة إنقر على تثبيت الوحدة وضع الرابط التالي في خانة عنوان URL

https://gitlab.com/mr_aljabry/foundryvtt-lang-ar-ar/-/raw/master/Core/module.json

إذا لم يعمل هذا الخيار قم بتحميل ملف core-AR.zip واستخرج محتوياته في ملف /resources/app/public/modules/
متى ما تم هذا، قم بتفعيل الوحدة في اعدادات عالمك الذي ترغب في استخدامها فيه وقم بتغير اللغة في الاعدادات.


# Localized Modules
The following list are modules that support Arabic language

# الوحدات المعرّبة
القائمة التالية تحتوي على وحدات تدعم اللغة العربية

# list in progress جاري العمل على القائمة
